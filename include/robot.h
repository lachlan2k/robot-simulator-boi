#pragma once

#include "renderer.h"
#include "updater.h"

namespace simulation {

class RobotModel;

class RobotDisplay : public Renderable {
    RobotModel& model;

public:
    RobotDisplay(RobotModel &model);
    void render(SDL2pp::Renderer& renderer) override;
};

struct RobotPose {
    double x;
    double y;
    double theta;

    RobotPose(double x, double y, double theta);
};

class RobotModel {
    double chassisWidth;
    double wheelSize;
    double speedScale;

    double leftOutput;
    double rightOutput;

    RobotPose currPose;

public:
    RobotModel(double chassisWidth = 8.0, double wheelSize = 4.125, double speedScale = 1.0);

    void update(unsigned int dT);
    
    RobotPose getCurrentPose();
    void setCurrentPose(const RobotPose& pose);

    void driveVector(double forward, double turn);
    void driveCoolVector(double forward, double turn);
};

class MasterController;

class Robot : public Updateable {
    friend class MasterController;

    RobotModel& model;
    RobotDisplay display;

public:
    Robot(RobotModel& model, RobotPose pose = RobotPose(0, 0, 0));
    void update(unsigned int dT);

    RobotModel& getModel();
};
};