#pragma once

#include <functional>
#include <vector>

namespace simulation {
class Updateable {
public:
    virtual void update(unsigned int dT) = 0;
    void addToUpdateList();
};

class UpdateController {
    static unsigned int lastCallTime;
    static unsigned int calculateDt();
    static std::vector<std::reference_wrapper<Updateable>>& getUpdateList();

public:
    static void updateAll();
    static void addToList(Updateable& updateable);
};
};