#pragma once

#include "path/path.hpp"
#include "okapi/api.hpp"
#include "robot.h"

namespace pathfollowing
{
class AdaptivePurePursuit
{
private:
  std::unique_ptr<okapi::IterativePosPIDController> straightController;
  std::unique_ptr<okapi::IterativePosPIDController> turnController;
  int mainLookahead;
  int lookahead;
  double lookaheadKf;
  path::Path *path;
  path::Point target;
  int direction;
  simulation::Robot &robot;

  okapi::QAngle calculateAngleError(okapi::QAngle pV, okapi::QAngle setpoint);
  double getHeadingAtPoint(int T);

public:
  AdaptivePurePursuit(
    simulation::Robot &robot,
      std::unique_ptr<okapi::IterativePosPIDController> straight,
      std::unique_ptr<okapi::IterativePosPIDController> turn,
      int lookahead, double lookaheadKf);

  void setPath(path::Path *path);

  void setLookahead(int lookahead);

  void loop();
  void ramseteLoop();
  void pureRamseteLoop();
};
} // namespace pathfollowing
