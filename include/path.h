#pragma once

#include "renderer.h"
#include "path/path.hpp"

namespace simulation {
class PathDisplay : public Renderable {
    path::Path *path;

public:
    PathDisplay(path::Path *path);
    void render(SDL2pp::Renderer& renderer) override;
};

class Path {
    PathDisplay display;

public:
    Path(path::Path *path);
};
}