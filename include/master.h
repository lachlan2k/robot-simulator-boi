#pragma once

#include "okapi/api.hpp"
using namespace okapi;
using namespace okapi::literals;

#include <SDL.h>
#include <SDL2pp/SDL.hh>
#include <SDL2pp/Window.hh>
#include <SDL2pp/Renderer.hh>

#include "path.h"
#include "robot.h"
#include "updater.h"
#include "renderer.h"
#include "path/path.hpp"
#include "adaptivepurepursuit.hpp"

namespace simulation {

class MasterController {
  const unsigned int fps;

  SDL2pp::SDL sdl;
  SDL2pp::Window window;
  SDL2pp::Renderer renderer;

  Robot robot;
  Path simulationPath;
  path::Path *path;
  pathfollowing::AdaptivePurePursuit& ppController;

  void tick();
  void init();

public:
  MasterController(pathfollowing::AdaptivePurePursuit& ppController, Robot &robot, path::Path *path, const unsigned int fps);

  void run();
  void setPath(const Path &path);
};
};