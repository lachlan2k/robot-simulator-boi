#pragma once

#include <SDL2pp/SDL.hh>
#include <SDL2pp/Renderer.hh>
#include <functional>
#include <vector>

namespace simulation {
class Renderable {
public:
    virtual void render(SDL2pp::Renderer& renderer) = 0;
    void addToRenderList();
};

class RenderController {
    static unsigned long lastCallTime;
    static std::vector<std::reference_wrapper<Renderable>>& getList();

public:
    static void renderAll(SDL2pp::Renderer& renderer);
    static void addToList(Renderable& renderable);
};
}