cmake_minimum_required(VERSION 3.7)

project(path_simulator)

# set(CMAKE_CXX_FLAGS "-Wno-pragmas")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wno-pragmas")
set(CMAKE_CXX_STANDARD 17)
set(SDL2DIR ${PROJECT_SOURCE_DIR}/extlib/SDL2-2.0.9/)
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

SET(SDL2PP_WITH_IMAGE OFF) # if you need SDL_image support
SET(SDL2PP_WITH_MIXER OFF) # if you need SDL_mixer support
SET(SDL2PP_WITH_TTF OFF) # if you need SDL_ttf support
add_subdirectory(extlib/libSDL2pp)

find_package(SDL2 REQUIRED COMPONENTS main)
include_directories(${SDL2_INCLUDE_DIRS})
include_directories(${SDL2PP_INCLUDE_DIRS})
include_directories(include) 

file(GLOB_RECURSE SOURCES "src/*.cpp")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

message(info libby bois ${SDL2_LIBS})

add_executable(path_simulator ${SOURCES})
target_link_libraries(path_simulator ${SDL2_LIBS} ${SDL2PP_LIBRARIES})