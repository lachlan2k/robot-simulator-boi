#include "robot.h"
#include <math.h>
#include <iostream>
#include <algorithm>
#include <utility>
#include <cmath>

#define ARRAY_LENGTH(x) (sizeof(x) / sizeof(x[0]))

namespace simulation {
RobotDisplay::RobotDisplay(RobotModel& model) : model(model) {}

RobotPose::RobotPose(double x, double y, double theta) : x(x), y(y), theta(theta) {}

void RobotDisplay::render(SDL2pp::Renderer& renderer) {
  auto currPose = model.getCurrentPose();
  renderer.SetDrawColor();

  // SDL2pp::Rect rect = {
  //   .x = currPose.x,
  //   .y = currPose.y,
  //   .w = 50,
  //   .h = 50
  // };

  // renderer.FillRect(rect);
  // return;

  auto x = currPose.x * 10;
  auto y = currPose.y * 10;

  auto makePoint = [&] (double theta, double spread = 30) {
    return SDL2pp::Point(
      static_cast<int>(x + std::sin(currPose.theta + theta) * spread) + 500,
      -static_cast<int>(y + std::cos(currPose.theta + theta) * spread) + 500
    );
  };

  SDL2pp::Point points[] = {
    makePoint(M_PI / 4),
    makePoint(M_PI * 3 / 4),
    makePoint(M_PI * 5 / 4),
    makePoint(M_PI * 7 / 4),
    makePoint(0),
    makePoint(M_PI / 4)
    // makePoint(0, 0),
    // makePoint(0, 50),
    // makePoint(M_PI/6, 45),
    // makePoint(0, 50),
    // makePoint(M_PI*2 - M_PI/2, 45),
    // makePoint(0, 50),
    // makePoint(0),
    // makePoint(M_PI / 4)
  };

  renderer.DrawLines(points, ARRAY_LENGTH(points));

  renderer.SetDrawColor(SDL2pp::Color(0, 100, 255));
  SDL2pp::Rect centerRect(500 + x - 3, 500 - y - 3, 6, 6);
  renderer.DrawRect(centerRect);
}

RobotModel::RobotModel(double chassisWidth, double wheelSize, double speedScale) :
  chassisWidth(chassisWidth),
  wheelSize(wheelSize),
  speedScale(speedScale),
  leftOutput(0),
  rightOutput(0),
  currPose(0, 0, M_PI * 0.5) {}

void RobotModel::update(unsigned int) {
  double deltaLeftWheel = leftOutput;
  double deltaRightWheel = rightOutput;

  double deltaCenter = (deltaLeftWheel + deltaRightWheel) / 2.0;
  double deltaTheta = (deltaLeftWheel - deltaRightWheel) / chassisWidth;

  double r1 = deltaTheta == 0 ? 0 : deltaCenter / deltaTheta;

  double relativeDeltaX = deltaTheta == 0 ? 0 : (r1 - r1 * std::cos(deltaTheta));
  double relativeDeltaY = deltaTheta == 0 ? deltaCenter : r1 * std::sin(deltaTheta);

  currPose.x += relativeDeltaX * std::cos(currPose.theta) + relativeDeltaY * std::sin(currPose.theta);
  currPose.y += relativeDeltaX * std::sin(currPose.theta) + relativeDeltaY * std::cos(currPose.theta);

  currPose.theta += deltaTheta;
}

void RobotModel::driveVector(double iforward, double iyaw) {
  const double forwardSpeed = std::clamp(iforward, -1.0, 1.0);
  const double yaw = std::clamp(iyaw, -1.0, 1.0);

  double left = forwardSpeed + yaw;
  double right = forwardSpeed - yaw;

  if (const double maxInputMag = std::max<double>(std::abs(left), std::abs(right));
      maxInputMag > 1) {
    left /= maxInputMag;
    right /= maxInputMag;
  }

  leftOutput = left;
  rightOutput = right;

  // printf("\n\nLeft: %.2f\nRight: %.2f\n", left, right);
}

// void RobotModel::driveCoolVector(double iforward, double iyaw) {
//   double forwardSpeed = std::clamp(iforward, -1.0, 1.0);
//   double yaw = std::clamp(iyaw, -1.0, 1.0);

//   auto getLeft = [&] () { return forwardSpeed + yaw; };
//   auto getRight = [&] () { return forwardSpeed - yaw; };

//   if (std::abs(getLeft()) > 1) {
//     auto maxForwardSpeed = 1.0 - std::abs(yaw);
//     forwardSpeed = 
//   }

//   leftOutput = getLeft();
//   rightOutput = getRight();
// }

void RobotModel::driveCoolVector(double iforward, double iyaw) {
  double yaw = std::clamp(iyaw, -1.0, 1.0);
  double maxForward = 1.0 - std::abs(yaw);
  double forward = std::clamp(iforward, -maxForward, maxForward);

  leftOutput = forward + yaw;
  rightOutput = forward - yaw;
}

RobotModel& Robot::getModel() {
  return model;
}

RobotPose RobotModel::getCurrentPose() {
  return currPose;
}

void RobotModel::setCurrentPose(const RobotPose& pose) {
  currPose = pose;
}

Robot::Robot(RobotModel& model, RobotPose pose) :
  model(model),
  display(model) {
    display.addToRenderList();
    this->addToUpdateList();
  }

void Robot::update(unsigned int dT) {
  model.update(dT);
}
}
