#include "master.h"

namespace simulation {
PathDisplay::PathDisplay(path::Path *path) : path(path) {
    this->addToRenderList();
}

void PathDisplay::render(SDL2pp::Renderer& renderer) {
    auto count = path->getResolution();
    const SDL2pp::Color black(0, 0, 0);
    const SDL2pp::Color red(255, 0, 0);

    std::vector<SDL2pp::Point> points;
    renderer.SetDrawColor(black);
    for (int i = 0; i < count; i++) {
        auto pathPoint = path->pointAt(i);

        int x = pathPoint.x.convert(inch) * 10;
        int y = -pathPoint.y.convert(inch) * 10;

        SDL2pp::Point sdlPoint(x + 500, y + 500);

        points.push_back(sdlPoint);
    }

    auto pointArray = &points[0];
    auto pointCount = points.size();

    renderer.DrawLines(pointArray, pointCount);

    auto lastPoint = path->pointAt(path->getLastLookingAt());
    int lastPointX = 500 + lastPoint.x.convert(inch) * 10;
    int lastPointY = 500 - lastPoint.y.convert(inch) * 10;

    SDL2pp::Rect lookingPointRect(lastPointX - 3, lastPointY - 3, 6, 6);
    SDL2pp::Point lookingPoint(lastPointX, lastPointY);

    renderer.SetDrawColor(red);
    renderer.DrawRect(lookingPointRect);
    renderer.DrawPoint(lookingPoint);
}

Path::Path(path::Path *path) : display(path) {}
}