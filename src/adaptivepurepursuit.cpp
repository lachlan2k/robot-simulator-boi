#include "adaptivepurepursuit.hpp"
#include <algorithm>

namespace pathfollowing
{
AdaptivePurePursuit::AdaptivePurePursuit(
	simulation::Robot &robot,
	std::unique_ptr<okapi::IterativePosPIDController> straight,
	std::unique_ptr<okapi::IterativePosPIDController> turn,
	int lookahead, double lookaheadKf) : 
										straightController(std::move(straight)),
										 turnController(std::move(turn)),
										 mainLookahead(lookahead),
										 lookahead(lookahead),
										 lookaheadKf(lookaheadKf),
										 direction(1),
										 robot(robot) {
											 turnController->setIntegratorReset(false);
										 }

void AdaptivePurePursuit::setPath(path::Path *path)
{
	this->path = path;
	int look = path->getLookahead();
	lookahead = (look < 0) ? mainLookahead : look;
}

void AdaptivePurePursuit::setLookahead(int lookahead)
{
	this->lookahead = lookahead;
}

using namespace okapi;

QAngle AdaptivePurePursuit::calculateAngleError(QAngle pV, QAngle setpoint)
{
	if (setpoint.convert(radian) < 1_pi && pV.convert(radian) > 1_pi)
	{
		pV = radian * (pV.convert(radian) - 2_pi);
	}
	double error = setpoint.convert(radian) - pV.convert(radian);
	return radian * (error > 1_pi ? error - 2_pi : error);
}

void AdaptivePurePursuit::loop()
{
	using namespace okapi;
	// system("cls");

	auto model = robot.getModel();
	auto pose = model.getCurrentPose();

	auto currHeading = QAngle(pose.theta);	

	// path::Point robotPosition = {odometry::currX, odometry::currY};
	path::Point robotPosition = {pose.x * inch, pose.y * inch};
	path::PointAndDistance closestPointAndDistance = path->getClosestPointAndDistance(robotPosition);

	int newLookahead = lookahead - (closestPointAndDistance.distance.convert(inch) * lookaheadKf);
	newLookahead = (newLookahead < 0) ? 1 : newLookahead;

	int requiredPosition = closestPointAndDistance.point.t;
	target = path->pointAt(requiredPosition + newLookahead);
	path->lookingAt(requiredPosition + newLookahead);

	double distTolookaheadPoint =
		sqrt(pow(target.x.convert(inch) - robotPosition.x.convert(inch), 2) + pow(target.y.convert(inch) - robotPosition.y.convert(inch), 2));

	straightController->setTarget(distTolookaheadPoint);

	double forwardPower = straightController->step(0);
	QAngle bearing =
		std::atan2((this->target.x.convert(inch) - robotPosition.x.convert(inch)),
				   (this->target.y.convert(inch) - robotPosition.y.convert(inch))) *
		radian;
			// constrain to 0-2pi

	// bearing = std::atan2(std::sin(bearing.convert(radian)), std::cos(bearing.convert(radian))) * radian;
	while (bearing.convert(radian) < 0)
	{
		bearing = radian * (bearing.convert(radian) + 2_pi);
	}
	QAngle currBearingAngle = std::atan2(std::sin(currHeading.convert(radian)), std::cos(currHeading.convert(radian))) * radian;
	// QAngle currBearingAngle = currHeading;

	// while (currBearingAngle.convert(radian) < 0) {
	// 	currBearingAngle = radian * (currBearingAngle.convert(radian) + 2_pi);
	// }

	// while (currBearingAngle.convert(radian) > 2_pi) {
	// 	currBearingAngle = radian * (currBearingAngle.convert(radian) - 2_pi);
	// }

	double angleError = calculateAngleError(currBearingAngle, bearing).convert(radian);
	// angleError = std::atan2(sin(angleError), cos(angleError));

	//if (target.t == path->getResolution())
	//{
	direction = 1;
	// if (angleError * 180.0 / 1_pi > 90)
	// {
	// 	angleError -= 1_pi;
	// 	// angleError *= -1;
	// 	direction *= -1;
	// }
	// else if (angleError * 180.0 / 1_pi < -90)
	// {
	// 	angleError += 1_pi;
	// 	// angleError *= -1;
	// 	direction *= -1;
	// }
	//}

	//turnController->setTarget(bearing.convert(degree));
	turnController->setTarget(0);

	// double turnPower = turnController->step(odometry::currAngle.convert(degree));
	// double turnPower = turnController->step(currHeading.convert(degree));
	//double turnPower = turnController->step(turnControllerPV.convert(degree));
	double turnPower = turnController->step(-angleError * 180 / 1_pi);

	direction = 1;

	forwardPower *= direction;

	// if (std::abs(angleError * 180 / 1_pi) > 90) forwardPower = 0;

	// const auto pointTurnThreshold = 100;
	// auto fabsAngleError = std::clamp(static_cast<double>(std::fabs(angleError * 180 / 1_pi)), 0.0, 180.0);

	// if (fabsAngleError >= pointTurnThreshold) {
	// 	auto angleOverThreshold = fabsAngleError - pointTurnThreshold;
	// 	auto multiplier = angleOverThreshold / (180 - pointTurnThreshold);
	// 	forwardPower *= multiplier;
	// }

	// printf("%f\n", bearing.convert(degree)); //lul that's big brain

	// printf("%.0f,%.5f,%.5f,%.5f,%.5f\n", bearing.convert(degree), this->target.x.convert(inch), this->target.y.convert(inch), robotPosition.x.convert(inch), robotPosition.y.convert(inch));
	// printf("Heading: %.0f\nTarget heading: %.0f\n\nHeading error: %0.f\n\nDistance to point: %.10f\n\nPoint at: %d\n\nPoint: (%.10f, %.10f)\nRobot: (%.10f, %.10f)\n\nClosest Point (%d) (%.10f, %.10f)", currHeading.convert(degree), bearing.convert(degree), angleError * 180 / 1_pi, distTolookaheadPoint, requiredPosition + newLookahead, this->target.x.convert(inch), this->target.y.convert(inch), robotPosition.x.convert(inch), robotPosition.y.convert(inch), closestPointAndDistance.point.t, closestPointAndDistance.point.x.convert(inch), closestPointAndDistance.point.y.convert(inch));

	// robot.getModel().driveVector(0.0, 1.0);
	robot.getModel().driveVector(forwardPower, turnPower);

	// drive::chassis.driveVector(direction * forwardPower, turnPower); // TODO CHASSIS MODEL IN CONSTRUCTOR INSTEAD OF HERE
}

void AdaptivePurePursuit::ramseteLoop()
{
	using namespace okapi;

	auto model = robot.getModel();
	auto pose = model.getCurrentPose();

	auto currHeading = QAngle(pose.theta);	

	path::Point robotPosition = {pose.x * inch, pose.y * inch};
	path::PointAndDistance closestPointAndDistance = path->getClosestPointAndDistance(robotPosition);

	int newLookahead = lookahead - (closestPointAndDistance.distance.convert(inch) * lookaheadKf);
	newLookahead = (newLookahead < 0) ? 1 : newLookahead;

	int requiredPosition = closestPointAndDistance.point.t;
	target = path->pointAt(requiredPosition + newLookahead);
	path->lookingAt(requiredPosition + newLookahead);

	double distTolookaheadPoint =
		sqrt(pow(target.x.convert(inch) - robotPosition.x.convert(inch), 2) + pow(target.y.convert(inch) - robotPosition.y.convert(inch), 2));

	straightController->setTarget(0);

	QAngle bearing =
		std::atan2((this->target.x.convert(inch) - robotPosition.x.convert(inch)),
				   (this->target.y.convert(inch) - robotPosition.y.convert(inch))) *
		radian;

	while (bearing.convert(radian) < 0)
	{
		bearing = radian * (bearing.convert(radian) + 2_pi);
	}

	QAngle currBearingAngle = std::atan2(std::sin(currHeading.convert(radian)), std::cos(currHeading.convert(radian))) * radian;

	double angleError = calculateAngleError(currBearingAngle, bearing).convert(radian);

	// NOTE
	// WE NEED TO FIGURE OUT A WAY TO TUNE THIS TO BE GOOD
	if (target.t >= path->getResolution() && distTolookaheadPoint < 1) {
		//printf("Doin the thing at the end");
		double headingAtPoint = getHeadingAtPoint(target.t);
		double bearingAtPoint = atan2(
			std::sin(headingAtPoint),
			std::cos(headingAtPoint)
		);
		// angleError = atan2(
		// 	sin(currBearingAngle.convert(radian) - bearingAtPoint),
		// 	cos(currBearingAngle.convert(radian) - bearingAtPoint)
		// );
		angleError = calculateAngleError(currBearingAngle, bearingAtPoint * radian).convert(radian);
		//printf("Angle Error: %1.2f\n", angleError * 180.0/1_pi);
		// angleError = atan2(
		// 	std::sin(angleError),
		// 	std::cos(angleError)
		// );
	} else {
		printf("target t: %d, path res: %d\n", target.t, path->getResolution());
	}

	turnController->setTarget(0);

	// ramsete math
	double zeta = 3.0;
	double beta = 1.0;

	double theta = currBearingAngle.convert(radian);

	double x_d = target.x.convert(inch);
	double y_d = target.y.convert(inch);

	double x_curr = robotPosition.x.convert(inch);
	double y_curr = robotPosition.y.convert(inch);

	double e1 = std::sin(theta) * (x_d - x_curr) + std::cos(theta) * (y_d - y_curr);
	double e2 = -std::cos(theta) * (x_d - x_curr) + std::sin(theta) * (y_d - y_curr);
	double e3 = angleError;

	double v_d = straightController->step(-distTolookaheadPoint);
	double w_d = turnController->step(-e3 * 180 / 1_pi);

	double k1 = 2 * zeta * sqrt((pow(w_d, 2) + beta * pow(v_d, 2)));
	double sinc = e3 == 0 ? 1 : std::sin(e3) / e3;

	double v_out = v_d * std::cos(e3) + k1 * e1;

	double w_out = w_d + beta * sinc * e2 + k1 * e3;
	
	robot.getModel().driveVector(v_out, w_out);
}

double AdaptivePurePursuit::getHeadingAtPoint(int T)
{
    auto point = path->pointAt(T);
    double targetPoseHeading;

    if (T - 1 >= 0)
    {
		//printf("Took target at end of path\n");
        auto preTarget = path->pointAt(T - 1);
		//printf("end: (%1.2f, %1.2f), preEnd: (%1.2f, %1.2f)\n", point.x.convert(inch), point.y.convert(inch), preTarget.x.convert(inch), preTarget.y.convert(inch));
        targetPoseHeading = atan2(point.x.convert(inch) - preTarget.x.convert(inch), point.y.convert(inch) - preTarget.y.convert(inch));

    }
    else
    {
		//printf("Took target at front of path\n");
        auto preTarget = path->pointAt(T + 1);
        targetPoseHeading = atan2(point.x.convert(inch) - preTarget.x.convert(inch), point.y.convert(inch) - preTarget.y.convert(inch));
    }

	//printf("target heading: %1.2f\n", targetPoseHeading * 180.0/1_pi);

    return targetPoseHeading;
}

// void AdaptivePurePursuit::pureRamseteLoop()
// {
// 	using namespace okapi;

// 	auto model = robot.getModel();
// 	auto pose = model.getCurrentPose();

// 	auto currHeading = QAngle(pose.theta);	

// 	path::Point robotPosition = {pose.x * inch, pose.y * inch};
// 	path::PointAndDistance closestPointAndDistance = path->getClosestPointAndDistance(robotPosition);

// 	int newLookahead = lookahead - (closestPointAndDistance.distance.convert(inch) * lookaheadKf);
// 	newLookahead = (newLookahead < 0) ? 1 : newLookahead;

// 	int requiredPosition = closestPointAndDistance.point.t;
// 	target = path->pointAt(requiredPosition + newLookahead);

// 	auto closestPoseHeading = getHeadingAtPoint(closestPointAndDistance.point.t) * radian;
// 	auto targetPoseHeading = getHeadingAtPoint(newLookahead + requiredPosition) * radian;

// 	// auto lookaheadHeadingChange = targetPoseHeading.convert(radian) - closestPoseHeading.convert(radian);
// 	// auto lookaheadHeadingChange = calculateAngleError(closestPoseHeading, targetPoseHeading).convert(radian);
// 	auto lookaheadHeadingChange = std::atan2(
// 		std::sin(closestPoseHeading.convert(radian) - targetPoseHeading.convert(radian)),
// 		std::cos(closestPoseHeading.convert(radian) - targetPoseHeading.convert(radian))
// 	);

// 	printf("Target Heading: %1.2f\n", targetPoseHeading.convert(degree));

// 	path->lookingAt(requiredPosition + newLookahead);

// 	double distTolookaheadPoint =
// 		sqrt(pow(target.x.convert(inch) - robotPosition.x.convert(inch), 2) + pow(target.y.convert(inch) - robotPosition.y.convert(inch), 2));

// 	straightController->setTarget(0);

// 	QAngle bearing =
// 		std::atan2((this->target.x.convert(inch) - robotPosition.x.convert(inch)),
// 				   (this->target.y.convert(inch) - robotPosition.y.convert(inch))) *
// 		radian;

// 	while (bearing.convert(radian) < 0)
// 	{
// 		bearing = radian * (bearing.convert(radian) + 2_pi);
// 	}

// 	QAngle currBearingAngle = std::atan2(std::sin(currHeading.convert(radian)), std::cos(currHeading.convert(radian))) * radian;

// 	double angleError = calculateAngleError(currBearingAngle, bearing).convert(radian);

// 	QAngle tempAtan2DesiredBearingBoi = std::atan2(std::sin(targetPoseHeading.convert(radian)), std::cos(targetPoseHeading.convert(radian))) * radian;
// 	double desiredHeadingAngleError = calculateAngleError(currBearingAngle, tempAtan2DesiredBearingBoi).convert(radian);
	
// 	printf("Desired Bearing Error: %1.2f\n", desiredHeadingAngleError * 180.0 / 1_pi); // when angle should be 0 it gets gayed
	
// 	turnController->setTarget(0); 

// 	// ramsete math
// 	double zeta = 0.0;
// 	double beta = 0.0;
// 	double kW = 0.3;

// 	double theta = currHeading.convert(radian);

// 	double x_d = target.x.convert(inch);
// 	double y_d = target.y.convert(inch);

// 	double x_curr = robotPosition.x.convert(inch);
// 	double y_curr = robotPosition.y.convert(inch);

// 	double e1 = std::sin(theta) * (x_d - x_curr) + std::cos(theta) * (y_d - y_curr);
// 	double e2 = std::cos(theta) * (x_d - x_curr) + std::sin(theta) * (y_d - y_curr);
// 	// double e3 = desiredHeadingAngleError;
// 	double e3 = 0;

// 	// double v_d = straightController->step(-distTolookaheadPoint);
// 	double v_d = 1.0;
// 	// double w_d = turnController->step(-e3 * 180 / 1_pi);
// 	// double w_d = targetPoseHeading.convert(degree);
// 	auto w_d = lookaheadHeadingChange * kW;

// 	double k1 = 2 * zeta * sqrt((pow(w_d, 2) + beta * pow(v_d, 2)));
// 	double sinc = e3 == 0 ? 1 : std::sin(e3) / e3;

// 	// double v_out = v_d * std::cos(e3) + k1 * e2;
// 	// double w_out = w_d + beta * sinc * e1 + k1 * e3;
// 	double v_out = v_d;
// 	double w_out = w_d;
	
// 	robot.getModel().driveVector(v_out, w_out);
// }

void AdaptivePurePursuit::pureRamseteLoop()
{
	using namespace okapi;

	auto model = robot.getModel();
	auto pose = model.getCurrentPose();

	auto currHeading = QAngle(pose.theta);	

	path::Point robotPosition = {pose.x * inch, pose.y * inch};
	path::PointAndDistance closestPointAndDistance = path->getClosestPointAndDistance(robotPosition);

	int newLookahead = lookahead - (closestPointAndDistance.distance.convert(inch) * lookaheadKf);
	newLookahead = (newLookahead < 0) ? 1 : newLookahead;

	int requiredPosition = closestPointAndDistance.point.t;
	target = path->pointAt(requiredPosition + newLookahead);

	auto closestPoseHeading = getHeadingAtPoint(closestPointAndDistance.point.t) * radian;
	auto targetPoseHeading = getHeadingAtPoint(newLookahead + requiredPosition) * radian;

	// auto lookaheadHeadingChange = targetPoseHeading.convert(radian) - closestPoseHeading.convert(radian);
	// auto lookaheadHeadingChange = calculateAngleError(closestPoseHeading, targetPoseHeading).convert(radian);
	auto lookaheadHeadingChange = std::atan2(
		std::sin(closestPoseHeading.convert(radian) - targetPoseHeading.convert(radian)),
		std::cos(closestPoseHeading.convert(radian) - targetPoseHeading.convert(radian))
	);

	printf("Target Heading: %1.2f\n", targetPoseHeading.convert(degree));

	path->lookingAt(requiredPosition + newLookahead);

	double distTolookaheadPoint =
		sqrt(pow(target.x.convert(inch) - robotPosition.x.convert(inch), 2) + pow(target.y.convert(inch) - robotPosition.y.convert(inch), 2));

	straightController->setTarget(0);

	QAngle bearing =
		std::atan2((this->target.x.convert(inch) - robotPosition.x.convert(inch)),
				   (this->target.y.convert(inch) - robotPosition.y.convert(inch))) *
		radian;

	while (bearing.convert(radian) < 0)
	{
		bearing = radian * (bearing.convert(radian) + 2_pi);
	}

	QAngle currBearingAngle = std::atan2(std::sin(currHeading.convert(radian)), std::cos(currHeading.convert(radian))) * radian;

	double angleError = calculateAngleError(currBearingAngle, bearing).convert(radian);

	QAngle tempAtan2DesiredBearingBoi = std::atan2(std::sin(targetPoseHeading.convert(radian)), std::cos(targetPoseHeading.convert(radian))) * radian;
	double desiredHeadingAngleError = calculateAngleError(currBearingAngle, tempAtan2DesiredBearingBoi).convert(radian);
	
	printf("Desired Bearing Error: %1.2f\n", desiredHeadingAngleError * 180.0 / 1_pi); // when angle should be 0 it gets gayed
	
	turnController->setTarget(0); 

	// ramsete math
	double zeta = 1.0;
	double beta = 0.0;
	// double kW = 0.3;

	double theta = currHeading.convert(radian);

	double x_d = target.x.convert(inch);
	double y_d = target.y.convert(inch);

	double x_curr = robotPosition.x.convert(inch);
	double y_curr = robotPosition.y.convert(inch);

	double e1 = std::sin(theta) * (x_d - x_curr) + std::cos(theta) * (y_d - y_curr);
	double e2 = std::cos(theta) * (x_d - x_curr) + std::sin(theta) * (y_d - y_curr);
	// double e3 = desiredHeadingAngleError;
	double e3 = calculateAngleError(currBearingAngle, targetPoseHeading).convert(radian);

	// double v_d = straightController->step(-distTolookaheadPoint);
	double v_d = 1.0;
	// double w_d = turnController->step(-e3 * 180 / 1_pi);
	// double w_d = targetPoseHeading.convert(degree);
	auto w_d = lookaheadHeadingChange;

	double k1 = 2 * zeta * sqrt((pow(w_d, 2) + beta * pow(v_d, 2)));
	double sinc = e3 == 0 ? 1 : std::sin(e3) / e3;

	double v_out = v_d * std::cos(e3) + k1 * e2;
	double w_out = w_d + beta * sinc * e1 + k1 * e3;
	// double v_out = v_d;
	// double w_out = w_d;
	
	robot.getModel().driveVector(v_out, w_out);
}
} // namespace pathfollowing
