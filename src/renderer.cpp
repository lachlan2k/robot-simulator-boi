#include "renderer.h"

namespace simulation {
void Renderable::addToRenderList() {
    RenderController::addToList(*this);
}
  
std::vector<std::reference_wrapper<Renderable>> &RenderController::getList() {
  static std::vector<std::reference_wrapper<Renderable>> list;
  return list;
}

void RenderController::renderAll(SDL2pp::Renderer &renderer) {
  auto &list = getList();

  renderer.SetDrawColor(255, 255, 255);
  renderer.Clear();

  for (auto renderable : list) {
    renderable.get().render(renderer);
  }

  renderer.Present();
}

void RenderController::addToList(Renderable &renderable) {
  auto& list = getList();
  list.push_back(renderable);
}
}