#include <SDL.h>
#include "updater.h"

namespace simulation {
void Updateable::addToUpdateList() {
  UpdateController::addToList(*this);
}

std::vector<std::reference_wrapper<Updateable>> &UpdateController::getUpdateList() {
  static std::vector<std::reference_wrapper<Updateable>> list;
  return list;
}

void UpdateController::addToList(Updateable &updateable) {
  auto &list = getUpdateList();
  list.push_back(updateable);
}

unsigned int UpdateController::lastCallTime = 0;

unsigned int UpdateController::calculateDt() {
  unsigned int now = SDL_GetTicks();
  unsigned int ret = now - lastCallTime;
  
  lastCallTime = now;

  if (ret <= 0) ret = 1;

  return ret;
}

void UpdateController::updateAll() {
  auto &list = getUpdateList();
  auto dt = calculateDt();

  for (auto updateable : list) {
    updateable.get().update(dt);
  }
}
}