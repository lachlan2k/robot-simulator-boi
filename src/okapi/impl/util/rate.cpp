/**
 * @author Ryan Benasutti, WPI
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <SDL.h>
#include "okapi/impl/util/rate.hpp"

namespace okapi {
Rate::Rate() = default;

void Rate::delay(const QFrequency ihz) {
  SDL_Delay(1000 / ihz.convert(Hz));
}

void Rate::delay(const int ihz) {
  SDL_Delay(1000 / ihz);
}

void Rate::delayUntil(const QTime itime) {
  SDL_Delay(itime.convert(millisecond));
}

void Rate::delayUntil(const uint32_t ims) {
  SDL_Delay(ims);
}
} // namespace okapi
