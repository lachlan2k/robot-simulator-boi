/**
 * @author Ryan Benasutti, WPI
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include "okapi/impl/control/iterative/iterativeControllerFactory.hpp"
#include "okapi/impl/filter/velMathFactory.hpp"
#include "okapi/impl/util/timeUtilFactory.hpp"

namespace okapi {
IterativePosPIDController
IterativeControllerFactory::posPID(const double ikP,
                                   const double ikI,
                                   const double ikD,
                                   const double ikBias,
                                   std::unique_ptr<Filter> iderivativeFilter) {
  return IterativePosPIDController(
    ikP, ikI, ikD, ikBias, TimeUtilFactory::create(), std::move(iderivativeFilter));
}

IterativeVelPIDController
IterativeControllerFactory::velPID(const double ikP,
                                   const double ikD,
                                   const double ikF,
                                   const double ikSF,
                                   const VelMathArgs &iparams,
                                   std::unique_ptr<Filter> iderivativeFilter) {
  return IterativeVelPIDController(ikP,
                                   ikD,
                                   ikF,
                                   ikSF,
                                   VelMathFactory::createPtr(iparams),
                                   TimeUtilFactory::create(),
                                   std::move(iderivativeFilter));
}
} // namespace okapi
