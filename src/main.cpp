#include "master.h"
#include "path/line.hpp"
#include "path/pathgroup.hpp"
#include "path/bezier.hpp"
#include "okapi/api/units/QLength.hpp"


int main(int, char *[]) {
  simulation::RobotModel model;
  simulation::Robot robot(model);

  // path::Line section1(
  //   {0_in, 0_in},
  //   {0_in, 20_in},
  //   // {0_in, 36_in},
  //   100, // the resolution (how many T there are)
  //   10   // the lookahead T, and it's basically the maximum distance the robot will look ahead
  // );

  // path::Line section2(
  //   {0_in, 20_in},
  //   {20_in, 0_in},
  //   // {0_in, 36_in},
  //   100, // the resolution (how many T there are)
  //   10   // the lookahead T, and it's basically the maximum distance the robot will look ahead
  // );

  // path::Line section3(
  //   {20_in, 20_in},
  //   {0_in, -20_in},
  //   // {0_in, 36_in},
  //   100, // the resolution (how many T there are)
  //   10   // the lookahead T, and it's basically the maximum distance the robot will look ahead
  // );

  // path::Line section4(
  //   {20_in, 0_in},
  //   {-20_in, 0_in},
  //   // {0_in, 36_in},
  //   100, // the resolution (how many T there are)
  //   10   // the lookahead T, and it's basically the maximum distance the robot will look ahead
  // );

  path::Line section1(
    {0_in, 0_in},
    {10_in, 20_in},
    // {0_in, 36_in},
    100, // the resolution (how many T there are)
    10   // the lookahead T, and it's basically the maximum distance the robot will look ahead
  );

  path::Line section2(
    {10_in, 20_in},
    {20_in, 0_in},
    // {0_in, 36_in},
    100, // the resolution (how many T there are)
    10   // the lookahead T, and it's basically the maximum distance the robot will look ahead
  );

  path::Line section3(
    {20_in, 20_in},
    {0_in, -20_in},
    // {0_in, 36_in},
    100, // the resolution (how many T there are)
    10   // the lookahead T, and it's basically the maximum distance the robot will look ahead
  );

  path::Line section4(
    {20_in, 0_in},
    {-20_in, 0_in},
    // {0_in, 36_in},
    100, // the resolution (how many T there are)
    10   // the lookahead T, and it's basically the maximum distance the robot will look ahead
  );

  path::PathGroup path2({ section1, section2, section3, section4 }, 400, 10);

  path::Line path(
    {0_in, 0_in},
    {30_in, 25_in},
    100,
    10
  );

  path::Bezier bezboi(
    {
      {0_in, 0_in},
      {30_in, 21_in},
      {25_in, 35_in},
      {15_in, 29_in}
    },
    1000, 10
  );


  path::Bezier notcircleboi(
    {
      {0_in, 0_in},
      {30_in, 0_in},
      {30_in, 30_in},
      {0_in, 30_in},
      {-30_in, 0_in},
      {-30_in, -30_in},
      {0_in, 30_in}
    },
    1000, 10
  );

  path::Bezier circleboi(
    {
      // {0_in, 0_in},
      // {15_in, 30_in},
      // {30_in, 0_in},
      // {15_in, -30_in},
      // {-30_in, 0_in},
      // {0_in, 0_in}


      // {0_in, 0_in},
      // {10_in, 0_in},
      // {10_in, 10_in},
      // {0_in, 10_in},
      // {0_in, 0_in}

      // {0_in, 0_in},
      // {10_in, 0_in},
      // {10_in, 10_in}, 
      // {0_in, 10_in},
      // {0_in, -10_in},
      // {-10_in, -10_in}

    //   {0_in, 0_in},
    //     {10_in, 0_in},
    //     {10_in, 10_in}, 
    //     {0_in, 10_in},
    //     {0_in, -10_in},
    //     {-10_in, -10_in},
    // {0_in, 0_in}

    {0_in, 0_in},
        {40_in, 0_in},
        {40_in, 40_in}, 
        {0_in, 40_in},
        {0_in, -40_in},
        {-40_in, -40_in},
        {-40_in, 0_in},
        {0_in, 0_in}
    },
    1000, 10
  );

  path::Bezier bb({
    {0_in, 0_in},
    {15_in, 5_in},
    {20_in, 20_in},
    {25_in, 35_in},
    {40_in, 40_in},
    {45_in, 30_in},
    {40_in, 0_in},
    {30_in, -25_in},
    {-20_in, -15_in},
    {-30_in, 10_in},
    {-20_in, 20_in}},
    5000, 100
  );
 
  auto p = bb;

  pathfollowing::AdaptivePurePursuit ppController(
    robot,
        std::make_unique<IterativePosPIDController>(0.3, 0.0, 0.0, 0.0, TimeUtilFactory::create()),
        std::make_unique<IterativePosPIDController>(0.03, 0.0, 1.0, 0.0, TimeUtilFactory::create()),
        1000, 0.1); // the number before the Kf is the lookahead global, but it will use the path's lookahead by default

  // pathfollowing::AdaptivePurePursuit ppController(
  //   robot,
  //       std::make_unique<IterativePosPIDController>(0.5, 0.0, 0.0, 0.0, TimeUtilFactory::create()),
  //       std::make_unique<IterativePosPIDController>(0.05, 0.0001, 0.0, 0.0, TimeUtilFactory::create()),
  //       10, 1.0); // the number before the Kf is the lookahead global, but it will use the path's lookahead by default


  ppController.setPath(&p);

  simulation::MasterController master(ppController, robot, &p, 60);

  // for (int i = 0; i < 400; i++) {
  //   auto point = path2.pointAt(i + 1);
  //   printf("%d: (%.10f, %.10f)\n", i, point.x.convert(inch), point.y.convert(inch));
  // }

  master.run();

  return 0;
}
