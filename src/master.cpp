#include "master.h"

namespace simulation {
MasterController::MasterController(pathfollowing::AdaptivePurePursuit& ppController, Robot& robot, path::Path* path, const unsigned int fps) :
  robot(robot),
  path(path),
  simulationPath(path),
  fps(fps),
  sdl(SDL_INIT_EVERYTHING),
  window("Robot Simulation", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1000, 1000, SDL_WINDOW_RESIZABLE),
  renderer(window, -1, SDL_RENDERER_ACCELERATED),
  ppController(ppController) {}

void MasterController::tick() {
    UpdateController::updateAll();
    RenderController::renderAll(renderer);
    ppController.ramseteLoop();

    SDL_Event event;

    if (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            SDL_Quit();
        }
    }
}

void MasterController::init() {
    RobotPose startingPose(0, 0, 0);
    robot.model.setCurrentPose(startingPose);
}

void MasterController::run() {
    init();

    while (1) {
        tick();

        SDL_Delay(1000 / fps);
    }
}
}