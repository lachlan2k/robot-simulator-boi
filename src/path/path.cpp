#include "path/path.hpp"
#include <stdio.h>
#include "robot.h"
#include <cmath>

namespace path
{
Point::Point(okapi::QLength x, okapi::QLength y, int t) : x(x), y(y), t(t) {}
Point::Point() : x(static_cast<double>(0)), y(static_cast<double> (0)) {} // gotta make it load and fuckin clear what ctor we wanna use

Path::Path(int resolution, int lookahead) : currT(0), resolution(resolution), lookAhead(lookahead) {}

Point Path::nextPoint(int lookahead)
{
    if (currT + lookahead > resolution)
    {
        currT = resolution;
    }
    else
    {
        currT += lookahead;
    }
    return pointAt(currT);
}

// PointAndDistance Path::getClosestPointAndDistance(Point inputPoint)
// {
//     using namespace okapi;

//     double x = inputPoint.x.convert(inch);
//     double y = inputPoint.y.convert(inch);

//     Point closestPoint = pointAt(0);
//     double distanceLeast = sqrt(pow((closestPoint.x.convert(inch) - x), 2) + pow((closestPoint.y.convert(inch) - y), 2));


//     for (int t = resolution; t >= 0; t--)
//     {
//         Point tempPoint = pointAt(t);
//         double distance = sqrt(pow((tempPoint.x.convert(inch) - x), 2) + pow((tempPoint.y.convert(inch) - y), 2));
//         if (distance < distanceLeast)
//         {
//             distanceLeast = distance;
//             closestPoint = tempPoint;
//         }
//     }
    
//     PointAndDistance closest = {closestPoint, distanceLeast * inch};
//     return closest;
// }

PointAndDistance Path::getClosestPointAndDistance(Point inputPoint)
{
    using namespace okapi;

    double x = inputPoint.x.convert(inch);
    double y = inputPoint.y.convert(inch);

    Point closestPoint = pointAt(0);
    double distanceLeast = sqrt(pow((closestPoint.x.convert(inch) - x), 2) + pow((closestPoint.y.convert(inch) - y), 2));

    auto checkPoint = [&] (int t) {
        Point tempPoint = pointAt(t);
        double distance = sqrt(pow((tempPoint.x.convert(inch) - x), 2) + pow((tempPoint.y.convert(inch) - y), 2));
        if (distance < distanceLeast)
        {
            distanceLeast = distance;
            closestPoint = tempPoint;
        }
    };

    for (int t = 0; t < 100; t++)
    {
        checkPoint(lastClosest + t);
        checkPoint(lastClosest - t);
    }
    
    lastClosest = closestPoint.t;
    PointAndDistance closest = {closestPoint, distanceLeast * inch};
    return closest;
}

void Path::lookingAt(int t) {
    lastLookingAt = t;
}

int Path::getLastLookingAt() {
    return lastLookingAt;
}

//
// Point pointAt(int T) = 0;

int Path::getResolution()
{
    return resolution;
}

int Path::getT()
{
    return this->currT;
}

void Path::setT(int t)
{
    currT = t;
}

int Path::getLookahead()
{
    return lookAhead;
}
} // namespace path
